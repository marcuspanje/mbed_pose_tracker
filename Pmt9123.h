#ifndef PMT9123_H
#define PMT9123_H

class Pmt9123 {
  public : 
    Pmt9123(PinName sda, PinName scl, PinName nReset): _i2c(sda, scl), _nReset(nReset) {} ;
    /** 
    * freq: i2c frequency
    * devAddr: i2c device address (0x33 based on datasheet)
    */
    void init(int freq, int devAddr);

    /**
    * reads xy motion
    * x: variable to store x value
    * y: variable to store y value
    * forceRead: 1 - read even if no motion. 0 - read only if there's motion
    */
    void read_xy(int& x, int& y, int forceRead);

    /**
    * performs an i2c read from an address
    * addr: register address
    * buf: buffer to store read value
    */
    void read(char addr, char* buf);

    /**
    * performs an i2c write to an address
    * addr: register address
    * buf: buffer containing data to be written
    */
    void write(char addr, char data);

    /**
    * tests basic read/write operations
    * returns true if tests pass, false if any fail
    */
    bool test();

    /** i2c device address */
    int dev_addr;

  private:
    I2C _i2c;
    DigitalOut _nReset;
    
};
#endif
