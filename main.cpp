#include "mbed.h"
#include <cassert>
#include <cmath>
#include <vector>
#include <string>
#include "Adns9500.h"
#include "LSM9DS1.h"
#include "Pmt9123.h"
#include "Quaternion.h"

using namespace std;


DigitalOut led1(LED1);
DigitalOut led2(LED2);
DigitalOut led3(LED3);
DigitalOut led4(LED4);


int i2c_freq = 200000; //freq of i2c clock in Hz
LSM9DS1 imu(p28, p27, i2c_freq, 0xD6, 0x3C); //sda, scl, i2c_freq, accelgyro addr, mag addr 
Pmt9123 mouse(p28, p27, p21); //sda, scl, nreset

Serial pc(USBTX, USBRX, 115200); //transmit pin receive pin, baudrate


//mouse calibration params
//double count_per_inch = 1400.0;
//double cmeter_per_count = 2.54/count_per_inch;
double count_to_cm = 0.00254;

//timers to regulate frequency
const double transmit_freq = 60.0; //data transmission freq to pc in Hz
Timer gyro_timer;
Timer serial_timer;

//quaternion filter parameters
double g_ms2 = 9.80665;
double alpha = 0.5; //imu complementary filter parameter

int main() {

    double pose[6]; // x,y,z,th_x,th_y,th_z
    for (int i = 0; i < 6; i++) {
         pose[i] = 0.0;
    }

    mouse.init(i2c_freq, 0x33);//i2c freq, dev address
    led1 = mouse.test();

    led2 = imu.begin();
    imu.calibrate();

    float gyro_dt = 0.0;
    int serialSendPeriod_ms = 1000*(1.0/transmit_freq);
    int dx;
    int dy;
    int gx, gy,gz,ax,ay,az;

    Quaternion qDelta(1,0,0,0);
    Quaternion qCmp(1,0,0,0);
    
    gyro_timer.start();
    serial_timer.start();

    while(1) {

        led2 = !led2;
        mouse.read_xy(dx, dy, 1);
        pose[0] += count_to_cm*dx;
        pose[1] -= count_to_cm*dy;

        imu.readTemp();
        imu.readGyro();

        gyro_dt = 0.000001*gyro_timer.read_us();
        gyro_timer.reset();

        //reorder axes based on desired convention
        gx = imu.calcGyro(imu.gy);
        gy = imu.calcGyro(imu.gz);
        gz = -imu.calcGyro(imu.gx);

        ax = g_ms2*imu.calcAccel(imu.ay);
        ay = g_ms2*imu.calcAccel(imu.az);
        az = -g_ms2*imu.calcAccel(imu.ax);
            

        //compute quaternion complementary filter
        //implementation based on imu lecture for ee267 at Stanford:
        //https://stanford.edu/class/ee267/lectures/lecture10.pdf 
        float l = sqrt((float)(sq(gx) + sq(gy) + sq(gz)));
        if (l >= 1e-8) {
          qDelta.setFromAngleAxis(
            gyro_dt * l, gx / l, gy / l, gz / l);
        }

        /* compute quaternion representing tilt of sensor */
        qCmp = Quaternion().multiply(qCmp, qDelta).normalize();
        /* compute complementary filter */
        Quaternion qa = Quaternion(0, ax, ay, az);
        Quaternion qa_inertial = qa.rotate(qCmp);
        double phi = acos(qa_inertial.q[2] / qa_inertial.length()) * RAD_TO_DEG;
        double n = sqrt(sq(qa_inertial.q[1]) + sq(qa_inertial.q[3]));
        Quaternion qTilt = Quaternion().setFromAngleAxis(
          (1 - alpha) * phi, -qa_inertial.q[3] / n, 0.0, qa_inertial.q[1] / n);

        qCmp = Quaternion().multiply(qTilt, qCmp).normalize();

        pose[2] = qCmp.q[0];
        pose[3] = qCmp.q[1];
        pose[4] = qCmp.q[2];
        pose[5] = qCmp.q[3];
        
        //send via usb
        if (serial_timer.read_ms() >= serialSendPeriod_ms) {
            serial_timer.reset();    
            pc.printf("%.4f %.4f %.4f %.4f %.4f %.4f\r\n", 
                pose[0], pose[1], pose[2], pose[3], pose[4], pose[5]);
        }
            
   } 

}

