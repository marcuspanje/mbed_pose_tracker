#ifndef ADNS9500_H
#define ADNS9500_H

#include "mbed.h"
#include <cassert>
#include <cmath>
#include <vector>
#include <string>

class Adns9500 {
    public:
        
        Adns9500(PinName mosi, PinName miso, PinName sclk, PinName ncs);
        /** 
        *performs spi read
        * addr: register address
        * returns: the byte read
        */
        char spi_read(int addr);
        
        /**
        * reads xy motion
        * x: variable storing x motion after read
        * y: variable storing y motion after read
        */
        void spi_read_xy(int &x, int &y);

        /** performs spi write
        * addr: register address
        * data: the buffer containing the data to be written
        */
        void spi_write(char addr, char data);  

        
        int spi_init();

    private:
        SPI _spi;
        DigitalOut _ncs;
};

#endif
